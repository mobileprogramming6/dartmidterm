import 'dart:io';

void main() {
  
  var strExprs = ('( ( ) 1 + 2 ) / 3');
  print('Sample Case: $strExprs');

  strExprs = tokenizing(strExprs);
  print('String through tokenizing(Infix) >> $strExprs');
  
  List listStr = addStrToList(strExprs);
  print('listStr = $listStr');

  toPostfix(listStr);
}

  String tokenizing(String strExprs) {
      // delete leading or trailing whitespace
      strExprs = delLeadingTrailingWhitspaceByTrim(strExprs);

      // replace whitespace by "," >> (1+2)/3
      strExprs = replaceAndDelWhitespace(strExprs);

      // delete ',' from strExprs
      strExprs = delSymbolFromString(strExprs); 
    
    return strExprs;
  }

  String delSymbolFromString(String strExprs) {
      strExprs = strExprs.replaceAll(',', "");
    return strExprs;
  }

  String replaceAndDelWhitespace(String strExprs) {
      final whitespaceRE = RegExp(r"\s+");
      strExprs = strExprs.split(whitespaceRE).join(",");
    return strExprs;
  }

  String delLeadingTrailingWhitspaceByTrim(String strExprs) {
      strExprs = strExprs.trim();
    return strExprs;
  }

  List<String> addStrToList (String strExprs) {
      strExprs = replaceAndDelWhitespace(strExprs);

      List<String> listStr = strExprs.split('');
    return listStr;
  }

void toPostfix(List listStr) {
  List operators = [];
  List postfix = [];

  String str;

  List symbols = ["+", "-", "*", "/", "%", "^", "(", ")"];

  int k=0;

  listStr.forEach((i) {
    str = i;

    print('str is $str');

    if (str=='(') {
      operators.add(str);
      print('add success');
      print(operators);
    } else if (str==')') {
      int x = operators.length-1;
      print('x = $x');
      do {
        postfix.add(operators[x]);
        // operators.removeAt(x);
      } while (operators[x] != '(');
      // operators.removeLast();
      // print(operators);
    }
    
    // index of listStr
    print('k = $k');
    k++;
  });
}